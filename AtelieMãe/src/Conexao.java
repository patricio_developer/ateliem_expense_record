
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.util.ArrayList;
import java.util.List;


public class Conexao {
    private ObjectContainer db = null;
    
    private void abrirBanco(){
        db = Db4oEmbedded.openFile("banco");
    
    }
    private void  fecharBanco(){
     db.close();
    }
    
    public void insertDespesa(Despesa d){
        abrirBanco();
        db.store(d);
        fecharBanco();
    
    }
    public List<Despesa> selectAllDespesa(){
        abrirBanco();
        ObjectSet listDespesa = db.queryByExample(Despesa.class);
        List<Despesa> lp = new ArrayList<>();
        for (Object listDespesa1 : listDespesa) {
            lp.add((Despesa)listDespesa1);
            
            
        }
        fecharBanco();
        return lp;
    
    }
    public Despesa selectDespesa(Despesa d){
    abrirBanco();
    ObjectSet result = db.queryByExample(d);
    Despesa despesa = (Despesa) result.next();
    fecharBanco();
    return despesa;
    }
    
    public void updateDespesa(int cod, String nitem, int npreco ){
        abrirBanco();
        Despesa d = new Despesa();
        d.setCod(cod);
        ObjectSet result = db.queryByExample(d);
        Despesa presult = (Despesa) result.next();
        presult.setItem(nitem);
        presult.setPreco(npreco);
        db.store(presult);
        fecharBanco();
   
    }
    public void deleleDespesa(int cod){
        abrirBanco();
        Despesa d = new Despesa();
        d.setCod(cod);
        ObjectSet result = db.queryByExample(d);
        Despesa presult = (Despesa) result.next();
        db.delete(presult);
        fecharBanco();
        
    
    
    }
    

    
}
