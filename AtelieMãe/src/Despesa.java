
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;


public class Despesa {
    private int cod;
    private String item;
    private float preco;
    private String moeda;
    private int quantidade;
    private LocalDate reg;
    private int total;
    

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public LocalDate getReg() {
        return reg;
    }

    public void setReg(LocalDate reg) {
        this.reg = reg;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    
    
}
